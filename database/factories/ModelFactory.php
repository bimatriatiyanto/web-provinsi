<?php

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Brackets\AdminAuth\Models\AdminUser::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->email,
        'password' => bcrypt($faker->password),
        'remember_token' => null,
        'activated' => true,
        'forbidden' => $faker->boolean(),
        'language' => 'en',
        'deleted_at' => null,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'last_login_at' => $faker->dateTime,
        
    ];
});/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Category::class, static function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence,
        'slug' => $faker->unique()->slug,
        'description' => $faker->text(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Post::class, static function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence,
        'content' => $faker->text(),
        'published_at' => $faker->date(),
        'enabled' => $faker->boolean(),
        'category_id' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Tag::class, static function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence,
        'slug' => $faker->unique()->slug,
        'description' => $faker->text(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Page::class, static function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence,
        'content' => $faker->text(),
        'slug_page' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Banner::class, static function (Faker\Generator $faker) {
    return [
        'title_banner' => $faker->sentence,
        'slug_banner' => $faker->sentence,
        'description_banner' => $faker->text(),
        'enabled' => $faker->boolean(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Setting::class, static function (Faker\Generator $faker) {
    return [
        'app_title' => $faker->sentence,
        'address' => $faker->sentence,
        'province_id' => $faker->randomNumber(5),
        'telephone' => $faker->sentence,
        'fax' => $faker->sentence,
        'email' => $faker->email,
        'balai_web_url' => $faker->sentence,
        'stageof_web_url' => $faker->sentence,
        'stamet_web_url' => $faker->sentence,
        'staklim_web_url' => $faker->sentence,
        'gaw_web_url' => $faker->sentence,
        'fb_url' => $faker->sentence,
        'twitter_url' => $faker->sentence,
        'instagram_url' => $faker->sentence,
        'youtube_url' => $faker->sentence,
        'meta_site_name' => $faker->sentence,
        'meta_description' => $faker->sentence,
        'meta_keyword' => $faker->sentence,
        'meta_twitter_site' => $faker->sentence,
        'google_analytics' => $faker->text(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
