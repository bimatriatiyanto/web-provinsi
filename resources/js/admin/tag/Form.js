import AppForm from '../app-components/Form/AppForm';

Vue.component('tag-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                title_tag:  '' ,
                slug_tag:  '' ,
                description_tag:  '' ,

            }
        }
    }

});
