import AppForm from '../app-components/Form/AppForm';

Vue.component('banner-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                title_banner:  '' ,
                slug_banner:  '' ,
                description_banner:  '' ,
                enabled:  false ,

            },
            mediaCollections: ['spanduk'],
        }
    }

});
