import AppForm from '../app-components/Form/AppForm';

Vue.component('page-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                title_page:  '' ,
                content_page:  '' ,
                slug_page:  '' ,
                description_page: '',

            }
        }
    }

});
