import AppForm from '../app-components/Form/AppForm';

Vue.component('post-form', {
    mixins: [AppForm],
    props: [
        'categories',
    ],
    data: function() {
        return {
            form: {
                title_post:  '' ,
                content_post:  '' ,
                description_post:  '' ,
                enabled:  false ,
                slug_post:  '' ,
                category:  '' ,
                tags:  '' ,

            },
            mediaCollections: ["thumbnail","berkas","gallery"],
        }
    }

});
