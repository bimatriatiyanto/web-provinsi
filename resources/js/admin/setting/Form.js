import AppForm from '../app-components/Form/AppForm';

Vue.component('setting-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                app_title:  '' ,
                address:  '' ,
                province:  '' ,
                telephone:  '' ,
                fax:  '' ,
                email:  '' ,
                balai_web_url:  '' ,
                stageof_web_url:  '' ,
                stamet_web_url:  '' ,
                staklim_web_url:  '' ,
                gaw_web_url:  '' ,
                fb_url:  '' ,
                twitter_url:  '' ,
                instagram_url:  '' ,
                youtube_url:  '' ,
                meta_site_name:  '' ,
                meta_description:  '' ,
                meta_keyword:  '' ,
                meta_twitter_site:  '' ,
                google_analytics:  '' ,

            }
        }
    }

});
