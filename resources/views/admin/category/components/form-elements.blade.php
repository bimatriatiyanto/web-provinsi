<div class="form-group row align-items-center" :class="{'has-danger': errors.has('title_category'), 'has-success': fields.title_category && fields.title_category.valid }">
    <label for="title_category" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.category.columns.title_category') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.title_category" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('title_category'), 'form-control-success': fields.title_category && fields.title_category.valid}" id="title_category" name="title_category" placeholder="{{ trans('admin.category.columns.title_category') }}">
        <div v-if="errors.has('title_category')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('title_category') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('slug_category'), 'has-success': fields.slug_category && fields.slug_category.valid }">
    <label for="slug_category" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.category.columns.slug_category') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.slug_category" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('slug_category'), 'form-control-success': fields.slug_category && fields.slug_category.valid}" id="slug_category" name="slug_category" placeholder="{{ trans('admin.category.columns.slug_category') }}">
        <div v-if="errors.has('slug_category')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('slug_category') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('description_category'), 'has-success': fields.description_category && fields.description_category.valid }">
    <label for="description_category" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.category.columns.description_category') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <textarea class="form-control" v-model="form.description_category" v-validate="''" id="description_category" name="description_category"></textarea>
        </div>
        <div v-if="errors.has('description_category')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('description_category') }}</div>
    </div>
</div>


