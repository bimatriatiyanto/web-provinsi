@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.setting.actions.index'))

@section('body')

    <setting-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('admin/settings') }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> {{ trans('admin.setting.actions.index') }}
                        <a class="btn btn-primary btn-spinner btn-sm pull-right m-b-0" href="{{ url('admin/settings/create') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.setting.actions.create') }}</a>
                    </div>
                    <div class="card-body" v-cloak>
                        <div class="card-block">
                            <form @submit.prevent="">
                                <div class="row justify-content-md-between">
                                    <div class="col col-lg-7 col-xl-5 form-group">
                                        <div class="input-group">
                                            <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                            <span class="input-group-append">
                                                <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-auto form-group ">
                                        <select class="form-control" v-model="pagination.state.per_page">

                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="100">100</option>
                                        </select>
                                    </div>
                                </div>
                            </form>

                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th class="bulk-checkbox">
                                            <input class="form-check-input" id="enabled" type="checkbox" v-model="isClickedAll" v-validate="''" data-vv-name="enabled"  name="enabled_fake_element" @click="onBulkItemsClickedAllWithPagination()">
                                            <label class="form-check-label" for="enabled">
                                                #
                                            </label>
                                        </th>

                                        <th is='sortable' :column="'id'">{{ trans('admin.setting.columns.id') }}</th>
                                        <th is='sortable' :column="'app_title'">{{ trans('admin.setting.columns.app_title') }}</th>
                                        <th is='sortable' :column="'address'">{{ trans('admin.setting.columns.address') }}</th>
                                        {{-- <th is='sortable' :column="'province_id'">{{ trans('admin.setting.columns.province_id') }}</th> --}}
                                        <th is='sortable' :column="'telephone'">{{ trans('admin.setting.columns.telephone') }}</th>
                                        <th is='sortable' :column="'fax'">{{ trans('admin.setting.columns.fax') }}</th>
                                        <th is='sortable' :column="'email'">{{ trans('admin.setting.columns.email') }}</th>
                                        {{-- <th is='sortable' :column="'balai_web_url'">{{ trans('admin.setting.columns.balai_web_url') }}</th>
                                        <th is='sortable' :column="'stageof_web_url'">{{ trans('admin.setting.columns.stageof_web_url') }}</th>
                                        <th is='sortable' :column="'stamet_web_url'">{{ trans('admin.setting.columns.stamet_web_url') }}</th>
                                        <th is='sortable' :column="'staklim_web_url'">{{ trans('admin.setting.columns.staklim_web_url') }}</th>
                                        <th is='sortable' :column="'gaw_web_url'">{{ trans('admin.setting.columns.gaw_web_url') }}</th>
                                        <th is='sortable' :column="'fb_url'">{{ trans('admin.setting.columns.fb_url') }}</th>
                                        <th is='sortable' :column="'twitter_url'">{{ trans('admin.setting.columns.twitter_url') }}</th>
                                        <th is='sortable' :column="'instagram_url'">{{ trans('admin.setting.columns.instagram_url') }}</th>
                                        <th is='sortable' :column="'youtube_url'">{{ trans('admin.setting.columns.youtube_url') }}</th>
                                        <th is='sortable' :column="'meta_site_name'">{{ trans('admin.setting.columns.meta_site_name') }}</th>
                                        <th is='sortable' :column="'meta_description'">{{ trans('admin.setting.columns.meta_description') }}</th>
                                        <th is='sortable' :column="'meta_keyword'">{{ trans('admin.setting.columns.meta_keyword') }}</th>
                                        <th is='sortable' :column="'meta_twitter_site'">{{ trans('admin.setting.columns.meta_twitter_site') }}</th> --}}

                                        <th></th>
                                    </tr>
                                    <tr v-show="(clickedBulkItemsCount > 0) || isClickedAll">
                                        <td class="bg-bulk-info d-table-cell text-center" colspan="22">
                                            <span class="align-middle font-weight-light text-dark">{{ trans('brackets/admin-ui::admin.listing.selected_items') }} @{{ clickedBulkItemsCount }}.  <a href="#" class="text-primary" @click="onBulkItemsClickedAll('/admin/settings')" v-if="(clickedBulkItemsCount < pagination.state.total)"> <i class="fa" :class="bulkCheckingAllLoader ? 'fa-spinner' : ''"></i> {{ trans('brackets/admin-ui::admin.listing.check_all_items') }} @{{ pagination.state.total }}</a> <span class="text-primary">|</span> <a
                                                        href="#" class="text-primary" @click="onBulkItemsClickedAllUncheck()">{{ trans('brackets/admin-ui::admin.listing.uncheck_all_items') }}</a>  </span>

                                            <span class="pull-right pr-2">
                                                <button class="btn btn-sm btn-danger pr-3 pl-3" @click="bulkDelete('/admin/settings/bulk-destroy')">{{ trans('brackets/admin-ui::admin.btn.delete') }}</button>
                                            </span>

                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection" :key="item.id" :class="bulkItems[item.id] ? 'bg-bulk' : ''">
                                        <td class="bulk-checkbox">
                                            <input class="form-check-input" :id="'enabled' + item.id" type="checkbox" v-model="bulkItems[item.id]" v-validate="''" :data-vv-name="'enabled' + item.id"  :name="'enabled' + item.id + '_fake_element'" @click="onBulkItemClicked(item.id)" :disabled="bulkCheckingAllLoader">
                                            <label class="form-check-label" :for="'enabled' + item.id">
                                            </label>
                                        </td>

                                    <td>@{{ item.id }}</td>
                                        <td>@{{ item.app_title }}</td>
                                        <td>@{{ item.address }}</td>
                                        {{-- <td>@{{ item.province_id }}</td> --}}
                                        <td>@{{ item.telephone }}</td>
                                        <td>@{{ item.fax }}</td>
                                        <td>@{{ item.email }}</td>
                                        {{-- <td>@{{ item.balai_web_url }}</td>
                                        <td>@{{ item.stageof_web_url }}</td>
                                        <td>@{{ item.stamet_web_url }}</td>
                                        <td>@{{ item.staklim_web_url }}</td>
                                        <td>@{{ item.gaw_web_url }}</td>
                                        <td>@{{ item.fb_url }}</td>
                                        <td>@{{ item.twitter_url }}</td>
                                        <td>@{{ item.instagram_url }}</td>
                                        <td>@{{ item.youtube_url }}</td>
                                        <td>@{{ item.meta_site_name }}</td>
                                        <td>@{{ item.meta_description }}</td>
                                        <td>@{{ item.meta_keyword }}</td>
                                        <td>@{{ item.meta_twitter_site }}</td> --}}

                                        <td>
                                            <div class="row no-gutters">
                                                <div class="col-auto">
                                                    <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                                </div>
                                                <form class="col" @submit.prevent="deleteItem(item.resource_url)">
                                                    <button type="submit" class="btn btn-sm btn-danger" title="{{ trans('brackets/admin-ui::admin.btn.delete') }}"><i class="fa fa-trash-o"></i></button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="row" v-if="pagination.state.total > 0">
                                <div class="col-sm">
                                    <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                                </div>
                                <div class="col-sm-auto">
                                    <pagination></pagination>
                                </div>
                            </div>

                            <div class="no-items-found" v-if="!collection.length > 0">
                                <i class="icon-magnifier"></i>
                                <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                                <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                                <a class="btn btn-primary btn-spinner" href="{{ url('admin/settings/create') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.setting.actions.create') }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </setting-listing>

@endsection
