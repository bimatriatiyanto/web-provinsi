<div class="card">
    <div class="card-header">
        <i class="fa fa-check"></i>
        {{ trans('admin.setting.columns.general') }}
    </div>
    <div class="card-block">
        <div class="form-group row align-items-center"
            :class="{ 'has-danger': errors.has('app_title'), 'has-success': fields.app_title && fields.app_title.valid }">
            <label for="app_title" class="col-form-label text-md-right"
                :class="isFormLocalized ? 'col-md-4' : 'col-md-2 col-lg-3'">{{ trans('admin.setting.columns.app_title') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8 col-lg-9'">
                <input type="text" v-model="form.app_title" v-validate="''" @input="validate($event)"
                    class="form-control"
                    :class="{
                        'form-control-danger': errors.has('app_title'),
                        'form-control-success': fields.app_title && fields
                            .app_title.valid
                    }"
                    id="app_title" name="app_title" placeholder="{{ trans('admin.setting.columns.app_title') }}">
                <div v-if="errors.has('app_title')" class="form-control-feedback form-text" v-cloak>
                    @{{ errors.first('app_title') }}</div>
            </div>
        </div>

        <div class="form-group row align-items-center"
            :class="{ 'has-danger': errors.has('address'), 'has-success': fields.address && fields.address.valid }">
            <label for="address" class="col-form-label text-md-right"
                :class="isFormLocalized ? 'col-md-4' : 'col-md-2 col-lg-3'">{{ trans('admin.setting.columns.address') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8 col-lg-9'">
                <input type="text" v-model="form.address" v-validate="''" @input="validate($event)"
                    class="form-control"
                    :class="{
                        'form-control-danger': errors.has('address'),
                        'form-control-success': fields.address && fields.address
                            .valid
                    }"
                    id="address" name="address" placeholder="{{ trans('admin.setting.columns.address') }}">
                <div v-if="errors.has('address')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('address') }}
                </div>
            </div>
        </div>

        <div class="form-group row align-items-center"
            :class="{ 'has-danger': errors.has('email'), 'has-success': fields.email && fields.email.valid }">
            <label for="email" class="col-form-label text-md-right"
                :class="isFormLocalized ? 'col-md-4' : 'col-md-2 col-lg-3'">{{ trans('admin.setting.columns.email') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8 col-lg-9'">
                <input type="text" v-model="form.email" v-validate="'email'" @input="validate($event)"
                    class="form-control"
                    :class="{
                        'form-control-danger': errors.has('email'),
                        'form-control-success': fields.email && fields.email
                            .valid
                    }"
                    id="email" name="email" placeholder="{{ trans('admin.setting.columns.email') }}">
                <div v-if="errors.has('email')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('email') }}
                </div>
            </div>
        </div>

        <div class="form-group row align-items-center"
            :class="{ 'has-danger': errors.has('fax'), 'has-success': fields.fax && fields.fax.valid }">
            <label for="fax" class="col-form-label text-md-right"
                :class="isFormLocalized ? 'col-md-4' : 'col-md-2 col-lg-3'">{{ trans('admin.setting.columns.fax') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8 col-lg-9'">
                <input type="text" v-model="form.fax" v-validate="''" @input="validate($event)" class="form-control"
                    :class="{ 'form-control-danger': errors.has('fax'), 'form-control-success': fields.fax && fields.fax.valid }"
                    id="fax" name="fax" placeholder="{{ trans('admin.setting.columns.fax') }}">
                <div v-if="errors.has('fax')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('fax') }}
                </div>
            </div>
        </div>

        <div class="form-group row align-items-center"
            :class="{ 'has-danger': errors.has('telephone'), 'has-success': fields.telephone && fields.telephone.valid }">
            <label for="telephone" class="col-form-label text-md-right"
                :class="isFormLocalized ? 'col-md-4' : 'col-md-2 col-lg-3'">{{ trans('admin.setting.columns.telephone') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8 col-lg-9'">
                <input type="text" v-model="form.telephone" v-validate="''" @input="validate($event)"
                    class="form-control"
                    :class="{
                        'form-control-danger': errors.has('telephone'),
                        'form-control-success': fields.telephone && fields
                            .telephone.valid
                    }"
                    id="telephone" name="telephone" placeholder="{{ trans('admin.setting.columns.telephone') }}">
                <div v-if="errors.has('telephone')" class="form-control-feedback form-text" v-cloak>
                    @{{ errors.first('telephone') }}
                </div>
            </div>
        </div>

        <div class="form-group row align-items-center"
            :class="{ 'has-danger': errors.has('province_id'), 'has-success': this.fields.province_id && this.fields.province_id
                    .valid }">
            <label for="province_id"
                class="col-form-label text-center col-md-4 col-lg-3">{{ trans('admin.setting.columns.province_id') }}</label>
            <div class="col-md-8 col-lg-9">

                <multiselect v-model="form.province" :options="{{ $province }}" :multiple="false"
                    track-by="id" label="name" tag-placeholder="{{ __('Select Province') }}"
                    placeholder="{{ __('Province') }}">
                </multiselect>

                <div v-if="errors.has('province_id')" class="form-control-feedback form-text" v-cloak>
                    @{{ errors.first('province_id') }}
                </div>
            </div>
        </div>


    </div>
</div>

<div class="card">
    <div class="card-header">
        <i class="fa fa-check"></i>{{ trans('admin.setting.columns.social_media') }}
    </div>
    <div class="card-block">
        <div class="form-group row align-items-center"
            :class="{ 'has-danger': errors.has('fb_url'), 'has-success': fields.fb_url && fields.fb_url.valid }">
            <label for="fb_url" class="col-form-label text-md-right"
                :class="isFormLocalized ? 'col-md-4' : 'col-md-2 col-lg-3'">{{ trans('admin.setting.columns.fb_url') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8 col-lg-9'">
                <input type="text" v-model="form.fb_url" v-validate="''" @input="validate($event)"
                    class="form-control"
                    :class="{
                        'form-control-danger': errors.has('fb_url'),
                        'form-contarol-success': fields.fb_url && fields.fb_url
                            .valid
                    }"
                    id="fb_url" name="fb_url" placeholder="{{ trans('admin.setting.columns.fb_url') }}">
                <div v-if="errors.has('fb_url')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('fb_url') }}
                </div>
            </div>
        </div>

        <div class="form-group row align-items-center"
            :class="{ 'has-danger': errors.has('twitter_url'), 'has-success': fields.twitter_url && fields.twitter_url.valid }">
            <label for="twitter_url" class="col-form-label text-md-right"
                :class="isFormLocalized ? 'col-md-4' : 'col-md-2 col-lg-3'">{{ trans('admin.setting.columns.twitter_url') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8 col-lg-9'">
                <input type="text" v-model="form.twitter_url" v-validate="''" @input="validate($event)"
                    class="form-control"
                    :class="{
                        'form-control-danger': errors.has('twitter_url'),
                        'form-control-success': fields.twitter_url && fields
                            .twitter_url.valid
                    }"
                    id="twitter_url" name="twitter_url"
                    placeholder="{{ trans('admin.setting.columns.twitter_url') }}">
                <div v-if="errors.has('twitter_url')" class="form-control-feedback form-text" v-cloak>
                    @{{ errors.first('twitter_url') }}
                </div>
            </div>
        </div>

        <div class="form-group row align-items-center"
            :class="{
                'has-danger': errors.has('instagram_url'),
                'has-success': fields.instagram_url && fields.instagram_url
                    .valid
            }">
            <label for="instagram_url" class="col-form-label text-md-right"
                :class="isFormLocalized ? 'col-md-4' : 'col-md-2 col-lg-3'">{{ trans('admin.setting.columns.instagram_url') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8 col-lg-9'">
                <input type="text" v-model="form.instagram_url" v-validate="''" @input="validate($event)"
                    class="form-control"
                    :class="{
                        'form-control-danger': errors.has('instagram_url'),
                        'form-control-success': fields.instagram_url && fields
                            .instagram_url.valid
                    }"
                    id="instagram_url" name="instagram_url"
                    placeholder="{{ trans('admin.setting.columns.instagram_url') }}">
                <div v-if="errors.has('instagram_url')" class="form-control-feedback form-text" v-cloak>
                    @{{ errors.first('instagram_url') }}
                </div>
            </div>
        </div>

        <div class="form-group row align-items-center"
            :class="{ 'has-danger': errors.has('youtube_url'), 'has-success': fields.youtube_url && fields.youtube_url.valid }">
            <label for="youtube_url" class="col-form-label text-md-right"
                :class="isFormLocalized ? 'col-md-4' : 'col-md-2 col-lg-3'">{{ trans('admin.setting.columns.youtube_url') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8 col-lg-9'">
                <input type="text" v-model="form.youtube_url" v-validate="''" @input="validate($event)"
                    class="form-control"
                    :class="{
                        'form-control-danger': errors.has('youtube_url'),
                        'form-control-success': fields.youtube_url && fields
                            .youtube_url.valid
                    }"
                    id="youtube_url" name="youtube_url"
                    placeholder="{{ trans('admin.setting.columns.youtube_url') }}">
                <div v-if="errors.has('youtube_url')" class="form-control-feedback form-text" v-cloak>
                    @{{ errors.first('youtube_url') }}
                </div>
            </div>
        </div>
    </div>
</div>

