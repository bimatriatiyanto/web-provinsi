<div class="card">
    <div class="card-header">
        <i class="fa fa-check"></i>{{ trans('admin.setting.columns.website_unit') }}
    </div>
    <div class="card-block">
        <div class="form-group row align-items-center"
            :class="{
                'has-danger': errors.has('balai_web_url'),
                'has-success': fields.balai_web_url && fields.balai_web_url
                    .valid
            }">
            <label for="balai_web_url" class="col-form-label text-md-right"
                :class="isFormLocalized ? 'col-md-4' : 'col-md-2 col-lg-3 col-lg-3'">{{ trans('admin.setting.columns.balai_web_url') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8 col-lg-9'">
                <input type="text" v-model="form.balai_web_url" v-validate="''" @input="validate($event)"
                    class="form-control"
                    :class="{
                        'form-control-danger': errors.has('balai_web_url'),
                        'form-control-success': fields.balai_web_url && fields
                            .balai_web_url.valid
                    }"
                    id="balai_web_url" name="balai_web_url"
                    placeholder="{{ trans('admin.setting.columns.balai_web_url') }}">
                <div v-if="errors.has('balai_web_url')" class="form-control-feedback form-text" v-cloak>
                    @{{ errors.first('balai_web_url') }}
                </div>
            </div>
        </div>

        <div class="form-group row align-items-center"
            :class="{
                'has-danger': errors.has('stamet_web_url'),
                'has-success': fields.stamet_web_url && fields.stamet_web_url
                    .valid
            }">
            <label for="stamet_web_url" class="col-form-label text-md-right"
                :class="isFormLocalized ? 'col-md-4' : 'col-md-2 col-lg-3 col-lg-3'">{{ trans('admin.setting.columns.stamet_web_url') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8 col-lg-9'">
                <input type="text" v-model="form.stamet_web_url" v-validate="''" @input="validate($event)"
                    class="form-control"
                    :class="{
                        'form-control-danger': errors.has('stamet_web_url'),
                        'form-control-success': fields.stamet_web_url &&
                            fields.stamet_web_url.valid
                    }"
                    id="stamet_web_url" name="stamet_web_url"
                    placeholder="{{ trans('admin.setting.columns.stamet_web_url') }}">
                <div v-if="errors.has('stamet_web_url')" class="form-control-feedback form-text" v-cloak>
                    @{{ errors.first('stamet_web_url') }}
                </div>
            </div>
        </div>

        <div class="form-group row align-items-center"
            :class="{
                'has-danger': errors.has('stageof_web_url'),
                'has-success': fields.stageof_web_url && fields.stageof_web_url
                    .valid
            }">
            <label for="stageof_web_url" class="col-form-label text-md-right"
                :class="isFormLocalized ? 'col-md-4' : 'col-md-2 col-lg-3 col-lg-3'">{{ trans('admin.setting.columns.stageof_web_url') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8 col-lg-9'">
                <input type="text" v-model="form.stageof_web_url" v-validate="''" @input="validate($event)"
                    class="form-control"
                    :class="{
                        'form-control-danger': errors.has('stageof_web_url'),
                        'form-control-success': fields.stageof_web_url &&
                            fields.stageof_web_url.valid
                    }"
                    id="stageof_web_url" name="stageof_web_url"
                    placeholder="{{ trans('admin.setting.columns.stageof_web_url') }}">
                <div v-if="errors.has('stageof_web_url')" class="form-control-feedback form-text" v-cloak>
                    @{{ errors.first('stageof_web_url') }}</div>
            </div>
        </div>

        <div class="form-group row align-items-center"
            :class="{
                'has-danger': errors.has('staklim_web_url'),
                'has-success': fields.staklim_web_url && fields.staklim_web_url
                    .valid
            }">
            <label for="staklim_web_url" class="col-form-label text-md-right"
                :class="isFormLocalized ? 'col-md-4' : 'col-md-2 col-lg-3 col-lg-3'">{{ trans('admin.setting.columns.staklim_web_url') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8 col-lg-9'">
                <input type="text" v-model="form.staklim_web_url" v-validate="''" @input="validate($event)"
                    class="form-control"
                    :class="{
                        'form-control-danger': errors.has('staklim_web_url'),
                        'form-control-success': fields.staklim_web_url &&
                            fields.staklim_web_url.valid
                    }"
                    id="staklim_web_url" name="staklim_web_url"
                    placeholder="{{ trans('admin.setting.columns.staklim_web_url') }}">
                <div v-if="errors.has('staklim_web_url')" class="form-control-feedback form-text" v-cloak>
                    @{{ errors.first('staklim_web_url') }}</div>
            </div>
        </div>

        <div class="form-group row align-items-center"
            :class="{ 'has-danger': errors.has('gaw_web_url'), 'has-success': fields.gaw_web_url && fields.gaw_web_url.valid }">
            <label for="gaw_web_url" class="col-form-label text-md-right"
                :class="isFormLocalized ? 'col-md-4' : 'col-md-2 col-lg-3 col-lg-3'">{{ trans('admin.setting.columns.gaw_web_url') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8 col-lg-9'">
                <input type="text" v-model="form.gaw_web_url" v-validate="''" @input="validate($event)"
                    class="form-control"
                    :class="{
                        'form-control-danger': errors.has('gaw_web_url'),
                        'form-control-success': fields.gaw_web_url && fields
                            .gaw_web_url.valid
                    }"
                    id="gaw_web_url" name="gaw_web_url"
                    placeholder="{{ trans('admin.setting.columns.gaw_web_url') }}">
                <div v-if="errors.has('gaw_web_url')" class="form-control-feedback form-text" v-cloak>
                    @{{ errors.first('gaw_web_url') }}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <i class="fa fa-check"></i>{{ trans('admin.setting.columns.meta') }}
    </div>
    <div class="card-block">
        <div class="form-group row align-items-center"
            :class="{
                'has-danger': errors.has('meta_site_name'),
                'has-success': fields.meta_site_name && fields.meta_site_name
                    .valid
            }">
            <label for="meta_site_name" class="col-form-label text-md-right"
                :class="isFormLocalized ? 'col-md-4' : 'col-md-2 col-lg-3'">{{ trans('admin.setting.columns.meta_site_name') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8 col-lg-9'">
                <input type="text" v-model="form.meta_site_name" v-validate="''" @input="validate($event)"
                    class="form-control"
                    :class="{
                        'form-control-danger': errors.has('meta_site_name'),
                        'form-control-success': fields.meta_site_name &&
                            fields.meta_site_name.valid
                    }"
                    id="meta_site_name" name="meta_site_name"
                    placeholder="{{ trans('admin.setting.columns.meta_site_name') }}">
                <div v-if="errors.has('meta_site_name')" class="form-control-feedback form-text" v-cloak>
                    @{{ errors.first('meta_site_name') }}
                </div>
            </div>
        </div>

        <div class="form-group row align-items-center"
            :class="{
                'has-danger': errors.has('meta_description'),
                'has-success': fields.meta_description && fields.meta_description
                    .valid
            }">
            <label for="meta_description" class="col-form-label text-md-right"
                :class="isFormLocalized ? 'col-md-4' : 'col-md-2 col-lg-3'">{{ trans('admin.setting.columns.meta_description') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8 col-lg-9'">
                <input type="text" v-model="form.meta_description" v-validate="''" @input="validate($event)"
                    class="form-control"
                    :class="{
                        'form-control-danger': errors.has('meta_description'),
                        'form-control-success': fields.meta_description &&
                            fields.meta_description.valid
                    }"
                    id="meta_description" name="meta_description"
                    placeholder="{{ trans('admin.setting.columns.meta_description') }}">
                <div v-if="errors.has('meta_description')" class="form-control-feedback form-text" v-cloak>
                    @{{ errors.first('meta_description') }}</div>
            </div>
        </div>

        <div class="form-group row align-items-center"
            :class="{
                'has-danger': errors.has('meta_keyword'),
                'has-success': fields.meta_keyword && fields.meta_keyword
                    .valid
            }">
            <label for="meta_keyword" class="col-form-label text-md-right"
                :class="isFormLocalized ? 'col-md-4' : 'col-md-2 col-lg-3'">{{ trans('admin.setting.columns.meta_keyword') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8 col-lg-9'">
                <input type="text" v-model="form.meta_keyword" v-validate="''" @input="validate($event)"
                    class="form-control"
                    :class="{
                        'form-control-danger': errors.has('meta_keyword'),
                        'form-control-success': fields.meta_keyword && fields
                            .meta_keyword.valid
                    }"
                    id="meta_keyword" name="meta_keyword"
                    placeholder="{{ trans('admin.setting.columns.meta_keyword') }}">
                <div v-if="errors.has('meta_keyword')" class="form-control-feedback form-text" v-cloak>
                    @{{ errors.first('meta_keyword') }}
                </div>
            </div>
        </div>

        <div class="form-group row align-items-center"
            :class="{
                'has-danger': errors.has('meta_twitter_site'),
                'has-success': fields.meta_twitter_site && fields.meta_twitter_site
                    .valid
            }">
            <label for="meta_twitter_site" class="col-form-label text-md-right"
                :class="isFormLocalized ? 'col-md-4' : 'col-md-2 col-lg-3'">{{ trans('admin.setting.columns.meta_twitter_site') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8 col-lg-9'">
                <input type="text" v-model="form.meta_twitter_site" v-validate="''" @input="validate($event)"
                    class="form-control"
                    :class="{
                        'form-control-danger': errors.has('meta_twitter_site'),
                        'form-control-success': fields
                            .meta_twitter_site && fields.meta_twitter_site.valid
                    }"
                    id="meta_twitter_site" name="meta_twitter_site"
                    placeholder="{{ trans('admin.setting.columns.meta_twitter_site') }}">
                <div v-if="errors.has('meta_twitter_site')" class="form-control-feedback form-text" v-cloak>
                    @{{ errors.first('meta_twitter_site') }}</div>
            </div>
        </div>
    </div>
</div>
