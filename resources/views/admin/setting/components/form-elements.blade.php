<div class="card">
    <div class="card-header">
        <i class="fa fa-check"></i>{{ trans('admin.setting.columns.analytics') }}
    </div>
    <div class="card-block">
        <div class="form-group row align-items-center"
            :class="{
                'has-danger': errors.has('google_analytics'),
                'has-success': fields.google_analytics && fields.google_analytics
                    .valid
            }">
            <label for="google_analytics" class="col-form-label text-md-right"
                :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.setting.columns.google_analytics') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8 col-lg-9'">
                <div>
                    <textarea class="form-control" v-model="form.google_analytics" v-validate="''" id="google_analytics"
                        name="google_analytics"></textarea>
                </div>
                <div v-if="errors.has('google_analytics')" class="form-control-feedback form-text" v-cloak>
                    @{{ errors.first('google_analytics') }}</div>
            </div>
        </div>
    </div>
</div>
