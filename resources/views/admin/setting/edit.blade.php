@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.setting.actions.edit', ['name' => $setting->email]))

@section('body')

    <div class="container-xl">

        <setting-form :action="'{{ $setting->resource_url }}'" :data="{{ $setting->toJson() }}" v-cloak inline-template>

            <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action"
                novalidate>

                <div class="row">
                    <div class="col">
                        @include('admin.setting.components.form-elements-left')
                    </div>

                    <div class="col">
                        @include('admin.setting.components.form-elements-right')
                    </div>
                </div>

                @include('admin.setting.components.form-elements')

                <button type="submit" class="btn btn-primary fixed-cta-button button-save" :disabled="submiting">
                    <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-save'"></i>
                    {{ trans('brackets/admin-ui::admin.btn.save') }}
                </button>
                <button type="submit" style="display: none" class="btn btn-success fixed-cta-button button-saved"
                    :disabled="submiting" :class="">
                    <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-check'"></i>
                    <span>{{ trans('brackets/admin-ui::admin.btn.saved') }}</span>
                </button>

            </form>

        </setting-form>


    </div>

@endsection
