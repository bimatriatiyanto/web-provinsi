<div class="form-group row align-items-center" :class="{'has-danger': errors.has('title_tag'), 'has-success': fields.title_tag && fields.title_tag.valid }">
    <label for="title_tag" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.tag.columns.title_tag') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.title_tag" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('title_tag'), 'form-control-success': fields.title_tag && fields.title_tag.valid}" id="title_tag" name="title_tag" placeholder="{{ trans('admin.tag.columns.title_tag') }}">
        <div v-if="errors.has('title_tag')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('title_tag') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('slug_tag'), 'has-success': fields.slug_tag && fields.slug_tag.valid }">
    <label for="slug_tag" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.tag.columns.slug_tag') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.slug_tag" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('slug_tag'), 'form-control-success': fields.slug_tag && fields.slug_tag.valid}" id="slug_tag" name="slug_tag" placeholder="{{ trans('admin.tag.columns.slug_tag') }}">
        <div v-if="errors.has('slug_tag')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('slug_tag') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('description_tag'), 'has-success': fields.description_tag && fields.description_tag.valid }">
    <label for="description_tag" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.tag.columns.description_tag') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <textarea class="form-control" v-model="form.description_tag" v-validate="''" id="description_tag" name="description_tag"></textarea>
        </div>
        <div v-if="errors.has('description_tag')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('description_tag') }}</div>
    </div>
</div>


