<div class="form-group row align-items-center" :class="{'has-danger': errors.has('title_page'), 'has-success': fields.title_page && fields.title_page.valid }">
    <label for="title_page" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.page.columns.title_page') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.title_page" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('title_page'), 'form-control-success': fields.title_page && fields.title_page.valid}" id="title_page" name="title_page" placeholder="{{ trans('admin.page.columns.title_page') }}">
        <div v-if="errors.has('title_page')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('title_page') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('slug_page'), 'has-success': fields.slug_page && fields.slug_page.valid }">
    <label for="slug_page" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.page.columns.slug_page') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.slug_page" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('slug_page'), 'form-control-success': fields.slug_page && fields.slug_page.valid}" id="slug_page" name="slug_page" placeholder="{{ trans('admin.page.columns.slug_page') }}">
        <div v-if="errors.has('slug_page')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('slug_page') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('content_page'), 'has-success': fields.content_page && fields.content_page.valid }">
    <label for="content_page" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.page.columns.content_page') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <wysiwyg class="form-control" v-model="form.content_page" v-validate="''" id="content_page" name="content_page" :config="mediaWysiwygConfig"></wysiwyg>
        </div>
        <div v-if="errors.has('content_page')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('content_page') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('description_page'), 'has-success': fields.description_page && fields.description_page.valid }">
    <label for="description_page" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.page.columns.description_page') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <textarea class="form-control" v-model="form.description_page" v-validate="''" id="description_page" name="description_page" ></textarea>
        </div>
        <div v-if="errors.has('description_page')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('description_page') }}</div>
    </div>
</div>



