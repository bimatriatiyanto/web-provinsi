<div class="form-group row align-items-center" :class="{'has-danger': errors.has('title_banner'), 'has-success': fields.title_banner && fields.title_banner.valid }">
    <label for="title_banner" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.banner.columns.title_banner') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.title_banner" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('title_banner'), 'form-control-success': fields.title_banner && fields.title_banner.valid}" id="title_banner" name="title_banner" placeholder="{{ trans('admin.banner.columns.title_banner') }}">
        <div v-if="errors.has('title_banner')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('title_banner') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('slug_banner'), 'has-success': fields.slug_banner && fields.slug_banner.valid }">
    <label for="slug_banner" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.banner.columns.slug_banner') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.slug_banner" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('slug_banner'), 'form-control-success': fields.slug_banner && fields.slug_banner.valid}" id="slug_banner" name="slug_banner" placeholder="{{ trans('admin.banner.columns.slug_banner') }}">
        <div v-if="errors.has('slug_banner')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('slug_banner') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('description_banner'), 'has-success': fields.description_banner && fields.description_banner.valid }">
    <label for="description_banner" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.banner.columns.description_banner') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <textarea class="form-control" v-model="form.description_banner" v-validate="''" id="description_banner" name="description_banner"></textarea>
        </div>
        <div v-if="errors.has('description_banner')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('description_banner') }}</div>
    </div>
</div>

<div class="form-check row" :class="{'has-danger': errors.has('enabled'), 'has-success': fields.enabled && fields.enabled.valid }">
    <div class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
        <input class="form-check-input" id="enabled" type="checkbox" v-model="form.enabled" v-validate="''" data-vv-name="enabled"  name="enabled_fake_element">
        <label class="form-check-label" for="enabled">
            {{ trans('admin.banner.columns.enabled') }}
        </label>
        <input type="hidden" name="enabled" :value="form.enabled">
        <div v-if="errors.has('enabled')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('enabled') }}</div>
    </div>
</div>


@if ($mode == 'create')
    @include('brackets/admin-ui::admin.includes.media-uploader', [
        'mediaCollection' => app(App\Models\Banner::class)->getMediaCollection('spanduk'),
        'label' => 'Banner File',
    ])
@else
    @include('brackets/admin-ui::admin.includes.media-uploader', [
        'mediaCollection' => app(App\Models\Banner::class)->getMediaCollection('spanduk'),
        'media' => $banner->getThumbs200ForCollection('spanduk'),
        'label' => 'Banner File',
    ])
@endif
