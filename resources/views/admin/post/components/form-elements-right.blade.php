<div class="card">
    <div class="card-header">
        <i class="fa fa-check"></i>{{ trans('admin.post.columns.description_post') }}
    </div>
    <div class="card-block">
        <div class="form-group row align-items-center"
            :class="{ 'has-danger': errors.has('description_post'), 'has-success': fields.description_post && fields
                    .description_post.valid }">
            <label for="description_post" class="col-form-label text-md-right"
                :class="isFormLocalized ? 'col-md-4' : 'col-md-2 col-lg-3'">{{ trans('admin.post.columns.description_post') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8 col-lg-9'">
                <div>
                    <textarea class="form-control" v-model="form.description_post" v-validate="''" id="description_post"
                        name="description_post"></textarea>
                </div>
                <div v-if="errors.has('description_post')" class="form-control-feedback form-text" v-cloak>
                    @{{ errors.first('description_post') }}</div>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <i class="fa fa-user"></i> {{ trans('admin.category.actions.index') }} </span>
    </div>

    <div class="card-block">
        <div class="form-group row align-items-center"
            :class="{ 'has-danger': errors.has('category_id'), 'has-success': this.fields.category_id && this.fields.category_id
                    .valid }">
            <label for="category_id"
                class="col-form-label text-center col-md-4 col-lg-3">{{ trans('admin.post.columns.category_id') }}</label>
            <div class="col-md-8 col-lg-9">

                <multiselect v-model="form.category" :options="{{ $categories }}" :multiple="false"
                    track-by="id" label="title_category" tag-placeholder="{{ __('Select Category') }}"
                    placeholder="{{ __('Category') }}">
                </multiselect>

                <div v-if="errors.has('category_id')" class="form-control-feedback form-text" v-cloak>
                    @{{ errors.first('category_id') }}
                </div>
            </div>
        </div>

    </div>
</div>

<div class="card">
    <div class="card-header">
        <span><i class="fa fa-tags"></i> {{ trans('admin.tag.actions.index') }} </span>
    </div>

    <div class="card-block">
        <div class="form-group row align-items-center"
            :class="{ 'has-danger': errors.has('tags'), 'has-success': this.fields.tags && this.fields.tags.valid }">
            <label for="author_id"
                class="col-form-label text-center col-md-4 col-lg-3">{{ trans('admin.tag.actions.index') }}</label>
            <div class="col-md-8 col-lg-9">

                <multiselect v-model="form.tags" :options="{{ $tags }}" :multiple="true" track-by="id"
                    label="title_tag" tag-placeholder="{{ __('Select Tags') }}" placeholder="{{ __('Tags') }}">
                </multiselect>

                <div v-if="errors.has('tags')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('tags') }}
                </div>
            </div>
        </div>

    </div>
</div>
