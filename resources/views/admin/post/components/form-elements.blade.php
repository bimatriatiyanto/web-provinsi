<div class="form-group row align-items-center" :class="{'has-danger': errors.has('title_post'), 'has-success': fields.title_post && fields.title_post.valid }">
    <label for="title_post" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.post.columns.title_post') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.title_post" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('title_post'), 'form-control-success': fields.title_post && fields.title_post.valid}" id="title_post" name="title_post" placeholder="{{ trans('admin.post.columns.title_post') }}">
        <div v-if="errors.has('title_post')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('title_post') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('content_post'), 'has-success': fields.content_post && fields.content_post.valid }">
    <label for="content_post" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.post.columns.content_post') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <wysiwyg class="form-control" v-model="form.content_post" v-validate="''" id="content_post" name="content_post" :config="mediaWysiwygConfig"></wysiwyg>
        </div>
        <div v-if="errors.has('content_post')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('content_post') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('slug_post'), 'has-success': fields.slug_post && fields.slug_post.valid }">
    <label for="slug_post" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.post.columns.slug_post') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.slug_post" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('slug_post'), 'form-control-success': fields.slug_post && fields.slug_post.valid}" id="slug_post" name="slug_post" placeholder="{{ trans('admin.post.columns.slug_post') }}">
        <div v-if="errors.has('slug_post')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('slug_post') }}</div>
    </div>
</div>


<div class="form-check row" :class="{'has-danger': errors.has('enabled'), 'has-success': fields.enabled && fields.enabled.valid }">
    <div class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
        <input class="form-check-input" id="enabled" type="checkbox" v-model="form.enabled" v-validate="''" data-vv-name="enabled"  name="enabled_fake_element">
        <label class="form-check-label" for="enabled">
            {{ trans('admin.post.columns.enabled') }}
        </label>
        <input type="hidden" name="enabled" :value="form.enabled">
        <div v-if="errors.has('enabled')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('enabled') }}</div>
    </div>
</div>


