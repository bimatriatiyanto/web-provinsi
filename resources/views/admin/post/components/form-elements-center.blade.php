@if ($mode == 'create')
    @include('brackets/admin-ui::admin.includes.media-uploader', [
        'mediaCollection' => app(App\Models\Post::class)->getMediaCollection('thumbnail'),
        'label' => 'Thumbnail',
    ])
@else
    @include('brackets/admin-ui::admin.includes.media-uploader', [
        'mediaCollection' => app(App\Models\Post::class)->getMediaCollection('thumbnail'),
        'media' => $post->getThumbs200ForCollection('thumbnail'),
        'label' => 'Thumbnail',
    ])
@endif

@if ($mode == 'create')
    @include('brackets/admin-ui::admin.includes.media-uploader', [
        'mediaCollection' => app(App\Models\Post::class)->getMediaCollection('berkas'),
        'label' => 'Document',
    ])
@else
    @include('brackets/admin-ui::admin.includes.media-uploader', [
        'mediaCollection' => app(App\Models\Post::class)->getMediaCollection('berkas'),
        'media' => $post->getThumbs200ForCollection('berkas'),
        'label' => 'Document',
    ])
@endif

@if ($mode == 'create')
    @include('brackets/admin-ui::admin.includes.media-uploader', [
        'mediaCollection' => app(App\Models\Post::class)->getMediaCollection('gallery'),
        'label' => 'Gallery',
    ])
@else
    @include('brackets/admin-ui::admin.includes.media-uploader', [
        'mediaCollection' => app(App\Models\Post::class)->getMediaCollection('gallery'),
        'media' => $post->getThumbs200ForCollection('gallery'),
        'label' => 'Gallery',
    ])
@endif
