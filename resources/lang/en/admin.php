<?php

return [
    'admin-user' => [
        'title' => 'Users',

        'actions' => [
            'index' => 'Users',
            'create' => 'New User',
            'edit' => 'Edit :name',
            'edit_profile' => 'Edit Profile',
            'edit_password' => 'Edit Password',
        ],

        'columns' => [
            'id' => 'ID',
            'last_login_at' => 'Last login',
            'first_name' => 'First name',
            'last_name' => 'Last name',
            'email' => 'Email',
            'password' => 'Password',
            'password_repeat' => 'Password Confirmation',
            'activated' => 'Activated',
            'forbidden' => 'Forbidden',
            'language' => 'Language',

            //Belongs to many relations
            'roles' => 'Roles',

        ],
    ],

    'category' => [
        'title_category' => 'Categories',

        'actions' => [
            'index' => 'Categories',
            'create' => 'New Category',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'title_category' => 'Title',
            'slug_category' => 'Slug',
            'description_category' => 'Description',

        ],
    ],

    'post' => [
        'title_post' => 'Posts',
        'upload' => 'Upload File',

        'actions' => [
            'index' => 'Posts',
            'create' => 'New Post',
            'edit' => 'Edit :name',
            'will_be_published' => 'Post will be published at',
        ],

        'columns' => [
            'id' => 'ID',
            'title_post' => 'Title',
            'content_post' => 'Content',
            'description_post' => 'Description',
            'enabled' => 'Enabled',
            'slug_post' => 'Slug',
            'category_id' => 'Category',
            'created_at' => 'Created At',

        ],
    ],

    'tag' => [
        'title_tag' => 'Tags',

        'actions' => [
            'index' => 'Tags',
            'create' => 'New Tag',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'title_tag' => 'Title',
            'slug_tag' => 'Slug',
            'description_tag' => 'Description',

        ],
    ],

    'page' => [
        'title_page' => 'Pages',

        'actions' => [
            'index' => 'Pages',
            'create' => 'New Page',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'title_page' => 'Title',
            'content_page' => 'Content',
            'slug_page' => 'Slug',
            'description_page' => 'Description',

        ],
    ],

    'banner' => [
        'title' => 'Banners',

        'actions' => [
            'index' => 'Banners',
            'create' => 'New Banner',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'title_banner' => 'Title',
            'slug_banner' => 'Slug',
            'description_banner' => 'Description',
            'enabled' => 'Enabled',

        ],
    ],

    'setting' => [
        'title' => 'Settings',

        'actions' => [
            'index' => 'Settings',
            'create' => 'New Setting',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'app_title' => 'App title',
            'address' => 'Address',
            'province_id' => 'Province',
            'telephone' => 'Telephone',
            'fax' => 'Fax',
            'email' => 'Email',
            'balai_web_url' => 'Balai web url',
            'stageof_web_url' => 'Stageof web url',
            'stamet_web_url' => 'Stamet web url',
            'staklim_web_url' => 'Staklim web url',
            'gaw_web_url' => 'Gaw web url',
            'fb_url' => 'Fb url',
            'twitter_url' => 'Twitter url',
            'instagram_url' => 'Instagram url',
            'youtube_url' => 'Youtube url',
            'meta_site_name' => 'Meta site name',
            'meta_description' => 'Meta description',
            'meta_keyword' => 'Meta keyword',
            'meta_twitter_site' => 'Meta twitter site',
            'google_analytics' => 'Google analytics',
            'general' => 'General',
            'website_unit' => 'Website UPT',
            'social_media' => 'Social Media',
            'meta' => 'Meta',
            'analytics' => 'Analytics',

        ],
    ],

    // Do not delete me :) I'm used for auto-generation
];
