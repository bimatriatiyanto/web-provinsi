<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Setting\BulkDestroySetting;
use App\Http\Requests\Admin\Setting\DestroySetting;
use App\Http\Requests\Admin\Setting\IndexSetting;
use App\Http\Requests\Admin\Setting\StoreSetting;
use App\Http\Requests\Admin\Setting\UpdateSetting;
use App\Models\Province;
use App\Models\Setting;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class SettingsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexSetting $request
     * @return array|Factory|View
     */
    public function index(IndexSetting $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Setting::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'app_title', 'address', 'province_id', 'telephone', 'fax', 'email', 'balai_web_url', 'stageof_web_url', 'stamet_web_url', 'staklim_web_url', 'gaw_web_url', 'fb_url', 'twitter_url', 'instagram_url', 'youtube_url', 'meta_site_name', 'meta_description', 'meta_keyword', 'meta_twitter_site'],

            // set columns to searchIn
            ['id', 'app_title', 'address', 'telephone', 'fax', 'email', 'balai_web_url', 'stageof_web_url', 'stamet_web_url', 'staklim_web_url', 'gaw_web_url', 'fb_url', 'twitter_url', 'instagram_url', 'youtube_url', 'meta_site_name', 'meta_description', 'meta_keyword', 'meta_twitter_site', 'google_analytics']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.setting.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.setting.create');

        return view('admin.setting.create',[
            'province' => Province::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreSetting $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreSetting $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();
        $sanitized['province_id'] = $request->getProvinceId();

        // Store the Setting
        $setting = Setting::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/settings'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/settings');
    }

    /**
     * Display the specified resource.
     *
     * @param Setting $setting
     * @throws AuthorizationException
     * @return void
     */
    public function show(Setting $setting)
    {
        $this->authorize('admin.setting.show', $setting);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Setting $setting
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Setting $setting)
    {
        $this->authorize('admin.setting.edit', $setting);

        $setting->load(['province']);


        return view('admin.setting.edit', [
            'setting' => $setting,
            'province' => new Province()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateSetting $request
     * @param Setting $setting
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateSetting $request, Setting $setting)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();
        $sanitized['province_id'] = $request->getProvinceId();

        // Update changed values Setting
        $setting->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/settings'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/settings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroySetting $request
     * @param Setting $setting
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroySetting $request, Setting $setting)
    {
        $setting->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroySetting $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroySetting $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    Setting::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
