<?php

namespace App\Http\Requests\Admin\Setting;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreSetting extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.setting.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'app_title' => ['nullable', 'string'],
            'address' => ['nullable', 'string'],
            'province' => ['required'],
            'telephone' => ['nullable', 'string'],
            'fax' => ['nullable', 'string'],
            'email' => ['nullable', 'email', 'string'],
            'balai_web_url' => ['nullable', 'string'],
            'stageof_web_url' => ['nullable', 'string'],
            'stamet_web_url' => ['nullable', 'string'],
            'staklim_web_url' => ['nullable', 'string'],
            'gaw_web_url' => ['nullable', 'string'],
            'fb_url' => ['nullable', 'string'],
            'twitter_url' => ['nullable', 'string'],
            'instagram_url' => ['nullable', 'string'],
            'youtube_url' => ['nullable', 'string'],
            'meta_site_name' => ['nullable', 'string'],
            'meta_description' => ['nullable', 'string'],
            'meta_keyword' => ['nullable', 'string'],
            'meta_twitter_site' => ['nullable', 'string'],
            'google_analytics' => ['nullable', 'string'],

        ];
    }

    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        //Add your code for manipulation with request data here

        return $sanitized;
    }

    public function getProvinceId()
    {
        if ($this->has('province')) {
            return $this->get('province')['id'];
        }
        return null;
    }
}
