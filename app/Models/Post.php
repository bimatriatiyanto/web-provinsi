<?php

namespace App\Models;

use Spatie\MediaLibrary\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Brackets\Media\HasMedia\ProcessMediaTrait;
use Brackets\Media\HasMedia\HasMediaThumbsTrait;
use Brackets\Media\HasMedia\AutoProcessMediaTrait;
use Brackets\Media\HasMedia\HasMediaCollectionsTrait;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Post extends Model implements HasMedia
{
    use ProcessMediaTrait;
    use AutoProcessMediaTrait;
    use HasMediaCollectionsTrait;
    use HasMediaThumbsTrait;

    protected $fillable = [
        'title_post',
        'content_post',
        'description_post',
        'enabled',
        'slug_post',
        'category_id',
        'admin_id',

    ];


    protected $dates = [
        'created_at',
        'updated_at',

    ];

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('thumbnail')
            ->useDisk('public')
            ->accepts('image/*')
            ->maxNumberOfFiles(5*1024)
            ->singleFile();

        $this->addMediaCollection('berkas')
            ->useDisk('public')
            ->accepts('application/pdf')
            ->maxNumberOfFiles(5*1024)
            ->singleFile();

        $this->addMediaCollection('gallery')
            ->useDisk('public')
            ->accepts('image/*')
            ->maxNumberOfFiles(5*1024);
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->autoRegisterThumb200();
    }

    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/posts/'.$this->getKey());
    }

    public function admin()
    {
        return $this->belongsTo(AdminUser::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
