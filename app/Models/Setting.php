<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
        'app_title',
        'address',
        'province_id',
        'telephone',
        'fax',
        'email',
        'balai_web_url',
        'stageof_web_url',
        'stamet_web_url',
        'staklim_web_url',
        'gaw_web_url',
        'fb_url',
        'twitter_url',
        'instagram_url',
        'youtube_url',
        'meta_site_name',
        'meta_description',
        'meta_keyword',
        'meta_twitter_site',
        'google_analytics',

    ];


    protected $dates = [
        'created_at',
        'updated_at',

    ];

    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/settings/'.$this->getKey());
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }
}
